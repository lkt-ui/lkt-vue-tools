import { LktObject } from 'lkt-ts-interfaces';
export declare function getSlots($slots: object, slotStartingPattern?: string): LktObject;
export declare function slotProvided($vm: any, name: string): boolean;
