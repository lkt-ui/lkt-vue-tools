function i(c, o) {
  const f = {};
  let n = {};
  if (c && (n = Object.assign(n, c)), o) {
    for (const e in n)
      e.indexOf(o) === 0 && (f[e.replace(o, "")] = n[e]);
    return f;
  }
  for (const e in n)
    f[e] = n[e];
  return f;
}
function r(c, o) {
  return !!c.$slots[o];
}
export {
  i as getSlots,
  r as slotProvided
};
