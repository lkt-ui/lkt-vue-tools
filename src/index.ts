import { LktObject } from 'lkt-ts-interfaces';

export function getSlots(
  $slots: object,
  slotStartingPattern?: string
): LktObject {
  const r: LktObject = {};
  let haystack: LktObject = {};
  if ($slots) {
    haystack = Object.assign(haystack, $slots);
  }

  if (slotStartingPattern) {
    for (const k in haystack) {
      if (k.indexOf(slotStartingPattern) === 0) {
        r[k.replace(slotStartingPattern, '')] = haystack[k];
      }
    }
    return r;
  }

  for (const k in haystack) {
    r[k] = haystack[k];
  }
  return r;
}

export function slotProvided($vm: any, name: string) {
  return !!$vm.$slots[name];
}
