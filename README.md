# LKT Vue Tools

## Methods

### getSlots

Retrieve all slots in a component $slots property.

If `slotStartingPattern` is given, only will return slots which name starts by that string.

#### Args
- $slots: object
- slotStartingPattern?: string

#### Returns
An object containing all slots

### slotProvided

Checks if client using this `$vm` Vue component had been defined any slot named `name`.

#### Args
- $vm: object
- name: string

#### Returns

Boolean